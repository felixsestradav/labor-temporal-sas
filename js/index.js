<script>
$(function(){
  $("[data-toggle='tooltip']").tooltip();
});
</script>
<script>
$(function(){
  $("[data-toggle='popover']").popover();
});
</script>
<script>
$(function(){
  $('.carousel').carousel({
    interval: 2000
  });
});

$(function(){
  $('#contacto').on('show.bs.modal', function (e){
    console.log('el modal contacto se está mostrando');
    $('#contacto').removeClass('btn-outline-succes');
    $('#contacto').prop('disabled', true);
           
  });


  $('#contacto').on('shown.bs.modal', function (e){
    console.log('el modal contacto se mostró');
  });
  
  
  $('#contacto').on('hidden.bs.modal', function (e){
    console.log('el modal contacto se está ocultando');
  });
  $('#contacto').on('hide.bs.modal', function (e){
    console.log('el modal contacto se ocultó');
    $('#contacto').prop('disabled', false);
  });
});

</script>